﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ChosenInMVC.Startup))]
namespace ChosenInMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
